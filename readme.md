# Fase Lunare || Sub-Orbital

Development of the Arduino code to run on the Sub-Orbital device.
The Arduino board is the [ARDUINO NANO 33 BLE](https://www.arduino.cc/en/Guide/NANO33BLE).

## How to run the code

Clone this repository, open the Arduino IDE and install the libraries documented in the **Requirements** section, open the **sub_orbital.ino** file located in dev/sub_orbital foder and upload it to the board.

## Project folder and main .ino

The project directory is dev/sub_orbital/, while the main .ino file is sub_orbital.ino

## Other Directories

- /assets: images and other display related stuffs
- /docs: contains hardware documentation, such as schematics
- /raphael: legacy code written by Raphaël Hoffman

## Project Structure

### General

**SubOrbital** is the main class of the project, it implements a singleton pattern so it can be instantiated only one time by the program.
Basically it manages all the UI parts (knobs, leds and the LCD screen) and the I/O of the board (CV, audio and MIDI).
The logic on how the Suborbital works is defined in classes that extend the **SubOrbitalProgram** class, the "program" of the board, for instance the **ProgramSequencer** class is the implementation of an eight step sequencer.
Every program class has two main methods, Setup and Loop methods, fired in the same way as any .ino scripts work.

### sub_orbital.ino

(tbd)

### SubOrbital

(tbd)

### SubOrbitalProgram

(tbd)

### SubOrbitalEncoder

(tbd)

### SubOrbitalKeyboard

(tbd)

### SubOrbitalLCD

(tbd)

### SubOrbitalLedStrip

(tbd)

### SubOrbitalPot

(tbd)

### SubOrbitalProgramSwitcher

(tbd)

## Requirements

### Board

- [Arduino nRF528x Boards (Mbed OS)](https://github.com/arduino/ArduinoCore-nRF528x-mbedos)

### Additional Libraries

#### BOARD UI
- [Adafruit Neopixel](https://github.com/adafruit/Adafruit_NeoPixel)
- [Adafruit SSD1306](https://github.com/adafruit/Adafruit_SSD1306)
- [Adafruit GFX Library](https://github.com/adafruit/Adafruit-GFX-Library)

#### INTEGRATED SENSORS
- [APDS9960 (gestures, proximity, color)](https://github.com/arduino-libraries/Arduino_APDS9960)
- [HTS221 (temperature, humidity)](https://github.com/arduino-libraries/Arduino_HTS221)

## Arduino Documentation

- [Getting Started with Arduino products](https://www.arduino.cc/en/Guide/HomePage)
- [Getting started with the Arduino NANO 33 BLE](https://www.arduino.cc/en/Guide/NANO33BLE)
- [Language Reference](https://www.arduino.cc/reference/en/)
- [Arduino nano 33](https://rootsaid.com/arduino-nano-33-ble-sense/)

## Tutorials / examples

- [gesture demo](https://create.arduino.cc/projecthub/tkhaled/arduino-nano-33-ble-sense-gestures-with-led-feedback-402a79?ref=part&ref_id=107215&offset=28)

## Theory

- Debouncing with Exponential Moving Average (link)[https://www.norwegiancreations.com/2015/10/tutorial-potentiometers-with-arduino-and-filtering/]

## Serial bus issue on Linux

It might happen that when you upload a sketch - after you have selected your board and the serial port -, you get an error Error opening serial port ... If you get this error, you need to set serial port permission.

Open Terminal and type:

	ls -l /dev/ttyACM*

you will get something like:

	crw-rw---- 1 root dialout 188, 0 5 apr 23.01 ttyACM0

The "0" at the end of ACM might be a different number, or multiple entries might be returned. The data we need is "dialout" (is the group owner of the file).

Now we just need to add our user to the group:

	sudo usermod -a -G dialout <username>

where <username> is your Linux user name. You will need to log out and log in again for this change to take effect.

## Credits

Board developed and assembled By Francesco Mulassano and Alessandro Comanzo.
Software developed by Raphaël Hoffman and Daniele Pagliero.

## License

(tbd)
