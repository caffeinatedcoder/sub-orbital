
#include "Arduino.h"
volatile static bool tick1 = false;
volatile static bool digiIn = false;
volatile static int tickNum;
//mbed::Ticker encoderTimer;
class orbitalEncoder {
private:
        const int encoderPin1 = 3;
        const int encoderPin2 = 6;


public:
    orbitalEncoder() :
        encoderPin2(6),
        encoderPin1(3)
    {

    };
    void init();
    static void in1_handler();
    static void scan();
    int getTicksNum();
};

