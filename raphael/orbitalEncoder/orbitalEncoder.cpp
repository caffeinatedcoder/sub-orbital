#include "orbitalEncoder.h"

void orbitalEncoder::init()
{
  pinMode(encoderPin1, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(encoderPin1), in1_handler, CHANGE);
}

void orbitalEncoder::in1_handler()
{
    tick1 = true;
   /* if (digiIn == true)
    {
        tickNum ++;
    }
    else
    {
        tickNum --;
    }
    digiIn = false;*/
}

void orbitalEncoder::scan()
{
        if (digitalRead(6) == HIGH && tick1 == true)
        {
            tickNum ++;
        }
        else if ( tick1 == true)
        {
            tickNum --;
        }
         tick1 = false;
}
int orbitalEncoder::getTicksNum()
{
    return tickNum;
}
