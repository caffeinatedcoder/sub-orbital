// main clock (1Khz)
void mainClock() {
    if (playing)
    {
        // new 16th note clock pulse
        bool clockPulse = false;  
        // detect end of period (that is calculte from the tempo global variable (see loop())-- else: increment the period ticks count 
        if (periodTicks>periodMs)
        {
          periodTicks = 0;
          clockPulse = true;
          // increment the playing steps number
          seqPos++;
        }
        else if (playing)
        {
          periodTicks++;
        }
        if (seqPos >=8)
        {
          seqPos = 0;
        }
        // activate the trig out when needed (durationMs is the suration of the trig in milliseconds)
        int durationMs = 10;
        if (seq1[pattern][seqPos] && periodTicks < durationMs)
        {
          *outPin1 = true;
        }
        else
        {
          *outPin1 = false;
        }
        if (seq2[pattern][seqPos] && periodTicks < durationMs)
        {
          *outPin2 = true;
        }
        else
        {
         *outPin2 = false;
        }
    }
    else
    {
      seqPos = 0;
      periodTicks = 0;
    }
}
