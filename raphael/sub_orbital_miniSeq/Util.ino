
void displaySubOrb()
{
  display.clearDisplay();
  display.setTextSize(2);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.println("SUB");
  display.setCursor(0, 20);     // Start at top-left corner
  display.println("ORBITAL");
  display.display();
}

void displayKnobValues(int Value1, int Value2)
{
  display.clearDisplay();
  display.setTextSize(2);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.println("PATT :");
  display.setCursor(90, 0);     // Start at top-left corner
  display.println(Value1 + 1);
  display.setCursor(0, 20);     // Start at top-left corner
  display.println("TRACK :");
  display.setCursor(90, 20);     // Start at top-left corner
  display.println(Value2 + 1);
  display.display();
}

void Menu(int instNumber, int patternNumber)
{
   if (instNumber < 330)
   {
      instNumber = 1;
   }
   else
   {
      instNumber = 0;
   }
   track = instNumber;
   pattern = patternNumber / 100;
   displayKnobValues(patternNumber / 100, instNumber);
}
// convert the number for the LED to correspong to the sequencer step's order (see util.ino)
uint8_t ledNumConvert(uint8_t num)
{
  uint8_t convertedNum = 0;
  if (num < 4)
  {
      convertedNum = 6 - num * 2;
  }
  else
  {
      convertedNum = 7 - (num - 4) * 2;
  }
  return convertedNum;
}

