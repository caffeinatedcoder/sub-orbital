// ledNumConvert(int ledNum) convert the number for the LED to correspong to the sequencer step's order (see util.ino)

void writeSeq(uint8_t buttonPressed)
{
  if (track == 0)
  {
    if (buttonPressed != REST)
    {
      seq1[pattern][buttonPressed] = !seq1[pattern][buttonPressed];
    }
    for (uint8_t i = 0; i < 8; i++)
    {
      if (seq1[pattern][i] == true)
      {
        if (i < 4)
        {
          LED_Strip->switchOnButtonLed(ledNumConvert(i), 255, 0, 0);
        }
        else
        {
          LED_Strip->switchOnButtonLed(ledNumConvert(i), 0, 0, 255);
        }
      }
      else
      {
        if (i < 4)
        {
          LED_Strip->switchOffButtonLed(ledNumConvert(i));
        }
        else
        {
          LED_Strip->switchOffButtonLed(ledNumConvert(i));
        }
      }
    }
  }
  else
  {
    if (buttonPressed != REST)
    {
      seq2[pattern][buttonPressed] = !seq2[pattern][buttonPressed];
    }
    for (uint8_t i = 0; i < 8; i++)
    {
      if (seq2[pattern][i] == true)
      {
        if (i < 4)
        {
          LED_Strip->switchOnButtonLed(ledNumConvert(i), 255, 0, 0);
        }
        else
        {
          LED_Strip->switchOnButtonLed(ledNumConvert(i), 0, 0, 255);
        }
      }
      else
      {
        if (i < 4)
        {
          LED_Strip->switchOffButtonLed(ledNumConvert(i));
        }
        else
        {
          LED_Strip->switchOffButtonLed(ledNumConvert(i));
        }
      }
    }
  }
  if (playing)
  {
    ledChase();
  }
}

// active LED on active step (you can change the color depending on the pos (1 to 4 or 4 to 8) and active state of the current step)

void ledChase()
{
  if (track == 0)
  {
    bool active = seq1[pattern][seqPos];
    if (seqPos < 4 && active)
    {
      LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 0, 255, 255);
    }
    else if (active)
    {
      LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 255, 255, 0);
    }
    else
    {
      if (seqPos < 4)
      {
        LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 0, 255, 255);
      }
      else
      {
        LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 255, 255, 0);
      }
    }
  }
  else if (track == 1)
  {
     bool active = seq2[pattern][seqPos];
     if (seqPos < 4 && active)
    {
      LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 0, 255, 255);
    }
    else if (active)
    {
      LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 255, 255, 0);
    }
    else
    {
      if (seqPos < 4)
      {
        LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 0, 255, 255);
      }
      else
      {
        LED_Strip->switchOnButtonLed(ledNumConvert(seqPos), 255, 255, 0);
      }
    }
  }
}

