/*  SEQ 0.01
 *  this example runs a two voices sequencer playing rythms on trig out 1 and 2
 *  - use the enccoder button to stop/start the sequencer
 *  - use the knobs for pattern and track selection
 *  - use the buttons to write a sequence
 *  - color for active/unactive/playing steps are in ledChase() and writeStep() functions (see sequencer.ino) 
 *  
 */
 
// external dedicated lib
#include <orbitalKeyboard.h>
#include <orbitalLedStrip.h>

// external lib
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// screen and outputs defines
#define SCREEN_WIDTH 128 
#define SCREEN_HEIGHT 64
#define OLED_RESET     13 
#define LED_NUMBER 27
#define TRIG_OUT_1 9
#define TRIG_OUT_2 10
#define REST     9 
#define ENCODER     8 

// objects declar.
orbitalLedStrip *LED_Strip = new orbitalLedStrip();
orbitalKeyboard *keyboard = new orbitalKeyboard();
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// mbed timer
mbed::Ticker timer; 

// declar of mbed pin to avoid erros when using in the ticked function (in process.ino)
PinName _pin1 = digitalPinToPinName(9);
mbed::DigitalOut *outPin1 = new mbed::DigitalOut(_pin1);
PinName _pin2 = digitalPinToPinName(10);
mbed::DigitalOut *outPin2 = new mbed::DigitalOut(_pin2); 

// global variables
int tempo = 120;
volatile int seqLength = 8;
volatile uint8_t seqPos = 0;
volatile bool pin = false; 
volatile long periodMs = 0;
long periodTicks = 0;
float frequency = 1000;
const int KNOB1 = A6;
const int KNOB2 = A3; 
volatile bool seq1[8][8] = {{false},{false}};
volatile bool seq2[8][8] = {{true},{true}};
volatile int track = 0;
volatile int pattern = 0;
volatile bool playing = false;


void setup() {
  pinMode(KNOB1, INPUT);
  pinMode(KNOB2, INPUT);
  pinMode(TRIG_OUT_1, OUTPUT);
  pinMode(TRIG_OUT_2, OUTPUT);
  keyboard->init();
  LED_Strip->init();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  displaySubOrb();
  delay(2000);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  pinMode(9,OUTPUT);
  // attach the mainCLock function to the mbed timer
  timer.attach(&mainClock, 0.5f / float(frequency)); 
  digitalWrite(8, HIGH);
}

void loop() 
{
   // tempo to period (millis) for a 16th note  
   periodMs = (1000 / (tempo / 60)) / 2;
   
   // (are scaled in the Menu function)
   int patternNumber = analogRead(KNOB1);
   int instNumber = analogRead(KNOB2);
   Menu(instNumber, patternNumber);
   uint8_t buttonPressed = keyboard->getKeyPressed();
   if (buttonPressed == ENCODER)
   {
     playing = !playing;
   }
   // read process is in mainCLock() (see process.ino)
   writeSeq(buttonPressed);

}
