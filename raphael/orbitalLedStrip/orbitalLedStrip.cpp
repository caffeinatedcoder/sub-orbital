#include "orbitalLedStrip.h"

void orbitalLedStrip::init()
{
  strip.begin();
  strip.show();
  strip.setBrightness(10);
  pinMode(ledPin, OUTPUT);
}
void orbitalLedStrip::switchOnMatrixRow(uint8_t rowNum, uint8_t R, uint8_t G, uint8_t B)
{
    uint8_t offset = 11 + rowNum;
    for(int i = 0; i < 4; i++)
    {
        strip.setPixelColor(offset + i * 4, strip.Color(R, G, B));
    }
    strip.show();
}
void orbitalLedStrip::switchOffMatrixRow(uint8_t rowNum)
{
    uint8_t offset = 11 + rowNum;
    for(int i = 0; i < 4; i++)
    {
        strip.setPixelColor(offset + i * 4, strip.Color(0, 0, 0));
    }
    strip.show();
}
void orbitalLedStrip::switchOnMatrixColumn(uint8_t columnNum, uint8_t R, uint8_t G, uint8_t B)
{
    uint8_t offset = 11 + columnNum * 4;
    for(int i = 0; i < 4; i++)
    {
        strip.setPixelColor(offset + i, strip.Color(R, G, B));
    }
    strip.show();
}
void orbitalLedStrip::switchOffMatrixColumn(uint8_t columnNum)
{
    uint8_t offset = 11 + columnNum * 4;
    for(int i = 0; i < 4; i++)
    {
        strip.setPixelColor(offset + i, strip.Color(0, 0, 0));
    }
    strip.show();
}
void orbitalLedStrip::switchOnMatrixLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B)
{
    uint8_t MatLedNum = ledNum + 11;
    strip.setPixelColor(MatLedNum, strip.Color(R, G, B)); //  Set pixel's color (in RAM)
    strip.show();
}
void orbitalLedStrip::switchOffMatrixLed(uint8_t ledNum)
{
    uint8_t MatLedNum = ledNum + 11;
    strip.setPixelColor(MatLedNum, strip.Color(0, 0, 0)); //  Set pixel's color (in RAM)
    strip.show();
}
void orbitalLedStrip::switchOnButtonLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B)
{
    uint8_t MatLedNum = ledNum + 3;
    strip.setPixelColor(MatLedNum, strip.Color(R, G, B)); //  Set pixel's color (in RAM)
    strip.show();
}
void orbitalLedStrip::switchOffButtonLed(uint8_t ledNum)
{
    uint8_t MatLedNum = ledNum + 3;
    strip.setPixelColor(MatLedNum, strip.Color(0, 0, 0)); //  Set pixel's color (in RAM)
    strip.show();
}
void orbitalLedStrip::switchOnKnobLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B)
{
    uint8_t MatLedNum = ledNum;
    strip.setPixelColor(MatLedNum, strip.Color(R, G, B)); //  Set pixel's color (in RAM)
    strip.show();
}
void orbitalLedStrip::switchOffKnobLed(uint8_t ledNum)
{
    uint8_t MatLedNum = ledNum;
    strip.setPixelColor(MatLedNum, strip.Color(0, 0, 0)); //  Set pixel's color (in RAM)
    strip.show();
}

