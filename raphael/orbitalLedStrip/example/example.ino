/*
 * This example use the sub-orbital LEDs, 
 * Three methods allow to control the LEDs matrix, the "button LEDs and the "knob Leds"
 * Two methods allow to control columns and rows of the LED matrix
 */
#include <orbitalLedStrip.h>
#define LED_NUMBER 27

orbitalLedStrip *LED_Strip = new orbitalLedStrip();

void setup() {
  // init the keyboard
  LED_Strip->init();
}
void loop() 
{
   // get the button pressed number (0-7) if a button is pressed
   for (int i = 0; i < 16; i++)   
   {
        LED_Strip->switchOnMatrixLed(i, 255, 0, 0);
        delay (250);
   }
   for (int i = 0; i < 16; i++)   
   {
       LED_Strip->switchOffMatrixLed(i);
       delay (250);
   }
   
   for (int i = 0; i < 8; i++)   
   {
       LED_Strip->switchOnButtonLed(i, 0, 0, 255);
       delay (250);
   }
   for (int i = 0; i < 8; i++)   
   {
       LED_Strip->switchOffButtonLed(i);
       delay (250);
   }
   
   for (int i = 0; i < 3; i++)   
   {
       LED_Strip->switchOnKnobLed(i, 0, 255, 0);
       delay (250);
   }
   for (int i = 0; i < 3; i++)   
   {
       LED_Strip->switchOffKnobLed(i);
       delay (250);
   }
   for (int i = 0; i < 4; i++)   
   {
       LED_Strip->switchOnMatrixRow(i, 0, 255, 0);
       delay (250);
   }
   for (int i = 0; i < 4; i++)   
   {
       LED_Strip->switchOffMatrixRow(i);
       delay (250);
   }
   for (int i = 0; i < 4; i++)   
   {
       LED_Strip->switchOnMatrixColumn(i, 0, 255, 0);
       delay (250);
   }
      for (int i = 0; i < 4; i++)   
   {
       LED_Strip->switchOffMatrixColumn(i);
       delay (250);
   }
}