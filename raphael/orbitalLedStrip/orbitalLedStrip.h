#ifndef orbitalLedStrip_h
#define orbitalLedStrip_h
#include "Arduino.h"
#include <Adafruit_NeoPixel.h>
#define LED_PIN    4
#define LED_COUNT 27
inline Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
class orbitalLedStrip
{
  public:
    void switchOnMatrixLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
    void switchOffMatrixLed(uint8_t ledNum);
    void switchOnButtonLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
    void switchOffButtonLed(uint8_t ledNum);
    void switchOnKnobLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
    void switchOffKnobLed(uint8_t ledNum);
    void switchOnMatrixRow(uint8_t rowNum, uint8_t R, uint8_t G, uint8_t B);
    void switchOffMatrixRow(uint8_t rowNum);
    void switchOnMatrixColumn(uint8_t columnNum, uint8_t R, uint8_t G, uint8_t B);
    void switchOffMatrixColumn(uint8_t columnNum);
    void init();
  private:
      uint8_t rgb_values[3];
      const int ledPin = LED_BUILTIN;
};
#endif
