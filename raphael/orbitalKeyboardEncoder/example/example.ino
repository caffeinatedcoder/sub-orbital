/*
 * This example use the sub-orbital buttons, 
 * Each time a button is pressed the getKeyPressed method return the number of the button
 * going from 0 to 7. Number 8 mean that no key are pressed currently.
 */
#include <orbitalKeyboard.h>
#define REST 9
// declare the keyboard
orbitalKeyboard *keyboard = new orbitalKeyboard();

void setup() {
  Serial.begin(9600);
  // init the keyboard
  keyboard->init();
}
void loop() 
{
   // get the button pressed number (0-7) if a button is pressed
   uint8_t buttonPressed = keyboard->getKeyPressed();
   if (buttonPressed != REST)
   {
    Serial.println(buttonPressed);
   }  
}