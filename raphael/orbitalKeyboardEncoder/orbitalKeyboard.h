/*
 * TCA8418 Driver
 * This library is adapted from the phishman's driver for arduino, https://github.com/phishman/TCA8418
 * The difference is that it is written to be easily ported to another microcontroller
 * Author: Henry Troutman
 */
#ifndef orbitalKeyboard_h
#define orbitalKeyboard_h
#include <Wire.h>
#include <SPI.h>
#include "Arduino.h"
#define KEYBOARD_ADDRESS 0x34
#define KEYBOARD_INT_PIN 7
#define KEYBOARD_STATE_REGISTER 0x04
#define KEYBOARD_FLAG_REGISTER 0x02
#define KEYBOARD_KEY_DOWN 0x80
#define KEYBOARD_KEY_MASK 0x7F
#define KEYBOARD_CODE_SHIFT 31
#define KEYBOARD_CODE_ALT 41
#define KEYBOARD_CODE_BACKSPACE 30
#define KEYBOARD_CODE_RETURN 40
#define KEYBOARD_CODE_SEND 46
#define KEYBOARD_CODE_LEFT_ARROW 47
#define KEYBOARD_CODE_UP_ARROW 39
#define KEYBOARD_CODE_DOWN_ARROW 48
#define KEYBOARD_CODE_RIGHT_ARROW 49
#define KEYBOARD_CODE_LEFT_SPACE 43
#define KEYBOARD_CODE_RIGHT_SPACE 44
#define KEYBOARD_CODE_UNUSED 42
const uint8_t KEYBOARD_CONFIG_COMMANDS[] = {0x1D,0x1F,0x1E,0xFF,0x1F,0x03,0x01,0xB9,0x02,0x0F};
    // Labels with placeholder characters for the control buttons
const char KEYBOARD_LABELS[] = {
 '.','1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
 'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',  'o',  'p',
 'a',  's',  'd',  'f',  'g',  'h',  'j',  'k',  'l',  '-',
 '#',  'z',  'x',  'c',  'v',  'b',  'n',  'm',  '^',  '*',
 '&',  '?',  ' ',  ' ',  '!','!',  '<',  ',',  '>'
};
void keyboard_configure(void);
void keyboard_clearFlag(void);
uint8_t keyboard_getState(void);
inline volatile bool keyInt = false;
void keyboard_ISR(void);
class orbitalKeyboard
{
  public:
    uint8_t getKeyPressed();
    void init();
  private:
};
/*
I2C Command Summary

Setup board (5x10 keypad)
write to Register, value
            0x1D, 0x1F
            0x1E, 0xFF
            0x1F, 0x03
            0x01, 0xB9

To get keys
read 1 byte from 0x04

To clear interrupt flag
write to register,value
            0x02, 0x0F

 */
#endif
