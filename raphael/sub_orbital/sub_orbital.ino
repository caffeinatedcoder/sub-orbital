
#include <orbitalKeyboard.h>
#include <orbitalLedStrip.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     8 // Reset pin # (or -1 if sharing Arduino reset pin)
#define REST 9
#define LED_NUMBER 27
#define TRIG_OUT_1 9
#define TRIG_OUT_2 10

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
const int KNOB1 = A6;
const int KNOB2 = A3; 
orbitalLedStrip *LED_Strip = new orbitalLedStrip();
orbitalKeyboard *keyboard = new orbitalKeyboard();
bool seq1[8][8] = {{false},{false}};
bool seq2[8][8] = {{true},{true}};
int track = 0;
int pattern = 0;

void setup() {
  Serial.begin(9600);
  pinMode(KNOB1, INPUT);
  pinMode(KNOB2, INPUT);
  pinMode(TRIG_OUT_1, OUTPUT);
  pinMode(TRIG_OUT_2, OUTPUT);
  keyboard->init();
  LED_Strip->init();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  displaySubOrb();
  delay(2000);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
}

void loop() 
{
   int patternNumber = analogRead(KNOB1);
   int instNumber = analogRead(KNOB2);
   Menu(instNumber, patternNumber);
   uint8_t buttonPressed = keyboard->getKeyPressed();
   writeSeq(buttonPressed);
}
