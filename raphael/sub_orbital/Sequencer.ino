void writeSeq(uint8_t buttonPressed)
{
  if (track == 0)
  {
    if (buttonPressed != REST)
    {
      seq1[pattern][buttonPressed] = true;
    }
    for (uint8_t i = 0; i < 8; i++)
    {
      if (seq1[pattern][i] == true)
      {
        if (i < 4)
        {
          LED_Strip->switchOnButtonLed(i * 2, 255, 0, 0);
        }
        else
        {
          LED_Strip->switchOnButtonLed((i - 4) * 2 + 1, 0, 255, 0);
        }
      }
      else
      {
        if (i < 4)
        {
          LED_Strip->switchOffButtonLed(i * 2);
        }
        else
        {
          LED_Strip->switchOffButtonLed((i - 4) * 2 + 1);
        }
      }
    }
  }
  else
  {
    if (buttonPressed != REST)
    {
      seq2[pattern][buttonPressed] = !seq2[pattern][buttonPressed];
    }
    for (uint8_t i = 0; i < 8; i++)
    {
      if (seq2[pattern][i] == true)
      {
        if (i < 4)
        {
          LED_Strip->switchOnButtonLed(i * 2, 255, 0, 0);
        }
        else
        {
          LED_Strip->switchOnButtonLed(i * 2 + 1, 0, 255, 0);
        }
      }
      else
      {
        if (i < 4)
        {
          LED_Strip->switchOffButtonLed(i * 2);
        }
        else
        {
          LED_Strip->switchOffButtonLed(i * 2 + 1);
        }
      }
    }
  }
}
  

