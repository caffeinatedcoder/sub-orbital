#include "orbitalKeyboard.h"

void orbitalKeyboard::init()
{
  Wire.begin();
  for(uint8_t i = 0;i < 9;i+=2){
    Wire.beginTransmission(KEYBOARD_ADDRESS);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i]);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i+1]);
    Wire.endTransmission();
  }
  pinMode(KEYBOARD_INT_PIN,INPUT);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  attachInterrupt(digitalPinToInterrupt(KEYBOARD_INT_PIN), keyboard_ISR, FALLING);
}


uint8_t keyboard_getState(void)
{
  uint8_t key;
  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_STATE_REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(KEYBOARD_ADDRESS, 1);    // request 1 bytes from slave device 0x34
  while (Wire.available()) { // slave may send less than requested
    key = Wire.read(); // receive a byte as character
  }
  return key;
}

void keyboard_clearFlag(void)
{
  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_FLAG_REGISTER);
  Wire.write(0x0F);
  Wire.endTransmission();
}

void keyboard_ISR(void)
{
  keyInt = true;
}
uint8_t orbitalKeyboard::getKeyPressed()
{
    int buttonNum = 8;
    if(keyInt){
    uint8_t key_code = keyboard_getState(); // Get first keycode from FIFO
    if(key_code & KEYBOARD_KEY_DOWN) {
      // This is where we can write to a screen or handle control keys
      switch (key_code&KEYBOARD_KEY_MASK)
      {
        case 3:
        buttonNum = 0;
        break;
        case 2:
        buttonNum = 1;
        break;
        case 1:
        buttonNum = 2;
        break;
        case 13:
        buttonNum = 3;
        break;
        case 12:
        buttonNum = 4;
        break;
        case 11:
        buttonNum = 5;
        break;
        case 22:
        buttonNum = 6;
        break;
        case 21:
        buttonNum = 7;
        break;
        default:
        buttonNum = 8;
        break;
      }
      keyInt = false;
      keyboard_clearFlag();
    }
  }
  return buttonNum;
}

