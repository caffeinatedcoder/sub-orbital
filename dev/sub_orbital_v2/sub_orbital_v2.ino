//ARDUINO NANO 33 BLE

// the main engine include file
#include "src/engine.h"

// the programs of the sub orbital
#include "src/programs/_include.h"


// engine is a singleton, so only one instance of this class is available
Engine *engine = Engine::Instance();

void setup() {
  // setup the engine to setup all the board facilities
  engine->Setup();

  // add programs to the board
  engine->programSwitcher->PushProgram(new ProgramIoTester());
  // engine->programSwitcher->PushProgram(new ProgramStartup());
  // engine->programSwitcher->PushProgram(new ProgramPotTester());
  // engine->programSwitcher->PushProgram(new ProgramEncoderTester());
  // engine->programSwitcher->PushProgram(new ProgramKbdTester());

}

void loop() {
  // engine has its own loop function.
  // nothing else to do in the main loop of the sketch
  engine->Loop();
}
