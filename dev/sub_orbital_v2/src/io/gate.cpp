#include "gate.h"

Gate::Gate() {
}

void Gate::Setup(uint8_t pin, uint8_t mode, voidFuncPtr interruptCallback) {
  _pin = pin;
  _mode = mode;
  _value = 0;
  calls = 0;

  pinMode(_pin, mode);

  if (interruptCallback) {
    attachInterrupt(digitalPinToInterrupt(pin), interruptCallback, RISING);
  }
}

void Gate::Out(int value) {
  if (_mode == OUTPUT) {
    _value = value;
    digitalWrite(_pin, value);
  }
}
