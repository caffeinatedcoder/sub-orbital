#ifndef __IO_GATE__
#define __IO_GATE__

#include <Arduino.h>

class Gate {
public:
  Gate();
  void Setup(uint8_t pin, uint8_t mode, voidFuncPtr interruptCallback = NULL);
  void Out(int value);
  int calls;
private:
  uint8_t _pin;
  uint8_t _mode;
  int _value;
};

#endif
