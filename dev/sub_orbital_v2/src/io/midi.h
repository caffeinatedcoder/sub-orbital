#ifndef __IO_MIDI__
#define __IO_MIDI__

#include "../../defines.h"
#ifdef SUB_ORBITAL

#include <Arduino.h>

const uint8_t NOTE_OFF = 0x80;
const uint8_t NOTE_ON = 0x90;
const uint8_t KEY_PRESSURE = 0xA0;
const uint8_t CC = 0xB0;
const uint8_t PROGRAM_CHANGE = 0xC0;
const uint8_t CHANNEL_PRESSURE = 0xD0;
const uint8_t PITCH_BEND = 0xE0;
const unsigned long headerResendTime = 1000; // send a new header every second

// https://www.instructables.com/id/Send-and-Receive-MIDI-with-Arduino/

class Midi {
public:
  Midi();
  void Setup(voidFuncPtr interruptCallback = NULL);
  void NoteOn(byte channel, byte note, byte velocity);
  void NoteOff(byte channel, byte note);
private:
  uint8_t _pin;
  uint8_t _mode;
  int _value;
  void sendMIDIHeader(uint8_t header);
  void sendMIDI(uint8_t messageType, uint8_t channel, uint8_t data1, uint8_t data2);
  void sendMIDI(uint8_t messageType, uint8_t channel, uint8_t data);
};

#endif
#endif
