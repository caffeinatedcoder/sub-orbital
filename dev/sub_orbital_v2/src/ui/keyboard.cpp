#include "keyboard.h"

Keyboard::Keyboard() {
}

void Keyboard::Setup(uint8_t defaultKeyboardMode, voidFuncPtr interruptCallback) {
  Wire.begin();
  for(uint8_t i = 0; i < 10; i+=2) {
    Wire.beginTransmission(KEYBOARD_ADDRESS);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i]);
    Wire.write(KEYBOARD_CONFIG_COMMANDS[i+1]);
    Wire.endTransmission();
  }

  pinMode(KEYBOARD_INT_PIN, INPUT);
  pinMode(KEYBOARD_RST_PIN, OUTPUT);
  digitalWrite(KEYBOARD_RST_PIN, HIGH);
  attachInterrupt(digitalPinToInterrupt(KEYBOARD_INT_PIN), interruptCallback, FALLING);

  _calls = 0;
  _keyboardChanged = false;
  _keyDown = -1;
  _keyUp = -1;
}

void Keyboard::Scan() {
  if(_keyboardChanged) {
    _calls ++;
    uint8_t key_code = getState();

    int index = -1;

    if(key_code & KEYBOARD_KEY_DOWN) {
      for (int i = 0; i < 9; i++) {
        if (KEYBOARD_BUTTON_MAP[i] == (key_code & KEYBOARD_KEY_MASK)) {
          index = i;
          break;
        }
      }
      _keyDown = index;
      _keyUp = -1;
    } else {
      for (int i = 0; i < 9; i++) {
        if (KEYBOARD_BUTTON_MAP[i] == (key_code & KEYBOARD_KEY_MASK)) {
          index = i;
          break;
        }
      }
      _keyDown = -1;
      _keyUp = index;
    }

    _keyboardChanged = false;
    clearFlag();
  }
}

void Keyboard::InterruptCallback() {
  _keyboardChanged = true;
}

uint8_t Keyboard::GetKeyDown() {
  return _keyDown;
}

uint8_t Keyboard::GetKeyUp() {
  return _keyUp;
}

int Keyboard::GetCalls() {
  return _calls;
}

void Keyboard::clearFlag() {
  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_FLAG_REGISTER);
  Wire.write(0x0F);
  Wire.endTransmission();
}

uint8_t Keyboard::getState() {
  uint8_t key;

  Wire.beginTransmission(KEYBOARD_ADDRESS);
  Wire.write(KEYBOARD_STATE_REGISTER);
  Wire.endTransmission();

  Wire.requestFrom(KEYBOARD_ADDRESS, 1);  // request 1 bytes from slave device
  while (Wire.available()) {              // slave may send less than requested
    key = Wire.read();                    // receive a byte as character
  }
  return key;
}
