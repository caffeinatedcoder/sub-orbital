#include "oled.h"

Oled::Oled() {
}

void Oled::Setup() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  _chartPos = 0;
  int chart[SCREEN_WIDTH];

  for (int i=0; i<SCREEN_WIDTH; i++) {
    chart[i] = 0;
  }

  _chart = chart;
  _scale = 1;
}

void Oled::RenderStartupScreen() {
  display.setTextSize(1);
  display.setCursor(30, 0);
  display.println("SUB ORBITAL V2");

  for (int x = 0; x < LOGO_WIDTH; x++) {
    for (int y = 0; y < LOGO_HEIGHT; y++) {
      if (LOGO[(y*LOGO_WIDTH)+x] == 1) {
        display.drawPixel(x+38, y+22, SSD1306_WHITE);
      }
    }
  }
}

void Oled::ClearDisplay() {
  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE);
  _scale = 1;
}

void Oled::Render() {
  display.display();
}

void Oled::DrawString(const char *string, int16_t x, int16_t y) {
  display.setTextSize(_scale);
  display.setCursor(x, y);
  display.println(string);
}

void Oled::DrawChar(uint8_t c, int16_t x, int16_t y) {
  display.setTextSize(_scale);
  display.setCursor(x, y);
  display.println(c);
}

void Oled::DrawStringLine(uint8_t line, const char *text) {
  display.setTextSize(_scale);
  display.setCursor(0, line*(LINE_HEIGHT*_scale));
  display.println(text);
}

void Oled::DrawParamLine(uint8_t line, const char *name, int value, int column) {
  int sw = SCREEN_WIDTH;
  int xOffset = 0;
  int columnSpace = 5;

  if (column > -1) {
    sw = sw / 2;
    xOffset = column * sw;
  }

  int fw = (FONT_WIDTH * _scale);
  if (value > 9 && value < 100) {
    fw *= 2;
  } else if (value >= 100 && value < 1000) {
    fw *= 3;
  } else if (value >= 1000 && value < 10000) {
    fw *= 4;
  } else if (value < 0 && value > -10) {
    fw *= 2;
  } else if (value <= -10 && value > -100) {
    fw *= 3;
  } else if (value <= -100 && value > -1000) {
    fw *= 4;
  }

  int x = xOffset + sw - fw;

  display.setCursor(x, line * LINE_HEIGHT * _scale);
  display.println(value);

  display.setTextSize(_scale);
  display.setCursor(xOffset + ((column > 0) ? columnSpace : 0), line * LINE_HEIGHT * _scale);
  display.println(name);
}

void Oled::DrawChart(int min, int max) {
  int y = 0;
  int yOffset = LINE_HEIGHT * _scale;
  int chartHeight = SCREEN_HEIGHT - yOffset;

  // draw chart data
  for (int x = 0; x < SCREEN_WIDTH; x++) {

    //y = (y + 1) % (SCREEN_HEIGHT - 22);

    y = chartHeight - map(_chart[x], min, max, 0, chartHeight);
    display.drawPixel(x, y+yOffset, SSD1306_WHITE);
  }

  // draw vertical line
  for (y = 0; y < chartHeight; y++) {
    display.drawPixel(_chartPos, y+yOffset, SSD1306_WHITE);
  }
}

void Oled::DrawPixel(int x, int y) {
  display.drawPixel(x, y, SSD1306_WHITE);
}

void Oled::DrawCircle(uint16_t x, uint16_t y, uint16_t radius) {
  display.drawCircle(x, y, radius, SSD1306_WHITE);
}

void Oled::PushChartValue(int val) {
  _chartPos = (_chartPos + 1) % SCREEN_WIDTH;
  _chart[_chartPos] = val;
}

void Oled::SetScale(int scale) {
  _scale = scale;
}
