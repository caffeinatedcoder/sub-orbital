#include "encoder.h"

Encoder::Encoder() {
}

void Encoder::Setup(int pin1, int pin2, voidFuncPtr interruptCallback) {
	_pin1 = pin1;
	_pin2 = pin2;

	_value = 0;
	_calls = 0;

	pinMode(_pin1, INPUT_PULLUP);
	pinMode(_pin2, INPUT_PULLUP);

	attachInterrupt(digitalPinToInterrupt(_pin1), interruptCallback, CHANGE);
}

void Encoder::InterruptCallback() {
	if (digitalRead(_pin2) == LOW) {
		return;
	}
	
	_calls ++;
	if (digitalRead(_pin1) == digitalRead(_pin2)) {
		_value++;
  } else {
		_value--;
  }
}

void Encoder::Reset(int value) {
	_calls = 0;
	_value = value;
}

int Encoder::GetValue() {
	return _value;
}

int Encoder::GetCalls() {
	return _calls;
}
