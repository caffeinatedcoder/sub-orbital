#ifndef __UI_OLED__
#define __UI_OLED__

#include "../../defines.h"
#ifdef SUB_ORBITAL

#define FONT_WIDTH 6
#define LINE_HEIGHT 10

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "logo.h"

inline Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET_PIN);

class Oled {
public:
  Oled();
  void Setup();
  void ClearDisplay();
  void Render();
  void RenderStartupScreen();
  void DrawString(const char *string, int16_t x, int16_t y);
  void DrawChar(uint8_t c, int16_t x, int16_t y);

  void DrawStringLine(uint8_t line, const char *text);
  void DrawParamLine(uint8_t line, const char *name, int value, int column = -1);

  void DrawChart(int min, int max);
  void DrawPixel(int x, int y);
  void DrawCircle(uint16_t x, uint16_t y, uint16_t radius);
  void PushChartValue(int val);
  void SetScale(int scale);
private:
  int _scale;
  int *_chart;
  int _chartPos;
};

#endif
#endif
