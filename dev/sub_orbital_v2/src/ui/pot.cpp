#include "pot.h"

Pot::Pot() {

}

void Pot::Setup(uint8_t pin, bool inverted, int minRef, int maxRef) {
  _pin = pin;
  _ema = 0;
  _max = 0;
  _minRef = minRef;
  _maxRef = maxRef;
  _delta = 0;
  _inverted = inverted;

  _ema = analogRead(_pin);
  if (_inverted) {
    _ema = _maxRef - _ema;
  }

  _max = _ema;
}

int Pot::ReadRaw() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = _maxRef - value;
  }
  _max = value > _max ? value : _max;
  return value;
}

int Pot::GetMax() {
  return _max;
}

int Pot::ReadAnalog() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = _maxRef - value;
  }

  // TODO: MUST BE OPTIMIZED (GOOD CLOCKWISE, BAD COUNTER CLOCKWISE)
  _delta = abs(value - _ema);

  _max = value > _max ? value : _max;
  _ema = exponential_moving_average(value, _ema);
  return _ema;
}

int Pot::ReadAsUnipolar() {
  return map(ReadAnalog(), _minRef, _maxRef, 0, 127);
}

int Pot::ReadAsBipolar() {
  return map(ReadAnalog(), _minRef, _maxRef, -64, 64);
}

int Pot::ReadAs128() {
  return ReadAsUnipolar();
}

int Pot::ReadAs64() {
  return map(ReadAnalog(), _minRef, _maxRef, 0, 64);
}

int Pot::ReadAs16() {
  return map(ReadAnalog(), _minRef, _maxRef, 0, 16);
}

int Pot::ReadAs8() {
  return map(ReadAnalog(), _minRef, _maxRef, 0, 8);
}

int Pot::ReadAs4() {
  return map(ReadAnalog(), _minRef, _maxRef, 0, 4);
}
