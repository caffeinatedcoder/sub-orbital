#ifndef __UI_LED_STRIP__
#define __UI_LED_STRIP__

#include "../../defines.h"
#ifdef SUB_ORBITAL

#include <Adafruit_NeoPixel.h>

inline Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

class LedStrip {
public:
  void Setup();
  void SwitchOnMatrixLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixLed(uint8_t ledNum);
  void SwitchOnButtonLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffButtonLed(uint8_t ledNum);
  void SwitchOnKnobLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffKnobLed(uint8_t ledNum);
  void SwitchOnMatrixRow(uint8_t rowNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixRow(uint8_t rowNum);
  void SwitchOnMatrixColumn(uint8_t columnNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffMatrixColumn(uint8_t columnNum);
  void SwitchOnIoLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
  void SwitchOffIoLed(uint8_t ledNum);
  void SwitchOffAll();
  void Show();

private:
  Adafruit_NeoPixel *_strip;
  uint8_t knobLedPositions[3] = {0, 1, 2};
  uint8_t buttonLedPositions[8] = {9, 7, 5, 3, 10, 8, 6, 4};
  uint8_t matrixLedPositions[16] = {
    11, 15, 19, 23,
    12, 16, 20, 24,
    13, 17, 21, 25,
    14, 18, 22, 26
  };
  uint8_t ioLedPositions[9] = {27, 28, 29, 30, 31, 32, 33, 34, 35};
  void setLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B);
};

#endif
#endif
