#include "led_strip.h"

void LedStrip::Setup() {
  strip.begin();
  strip.setBrightness(10);
}

void LedStrip::SwitchOnMatrixRow(uint8_t rowNum, uint8_t R, uint8_t G, uint8_t B) {
  if (rowNum >= 4 || rowNum < 0) return;
  for (int i = 0; i<4; i++) {
    SwitchOnMatrixLed(rowNum*4+i, R, G, B);
  }
}

void LedStrip::SwitchOffMatrixRow(uint8_t rowNum) {
  SwitchOnMatrixRow(rowNum, 0, 0, 0);
}

void LedStrip::SwitchOnMatrixColumn(uint8_t columnNum, uint8_t R, uint8_t G, uint8_t B) {
  if (columnNum >= 4 || columnNum < 0) return;
  for (int i = 0; i<4; i++) {
    SwitchOnMatrixLed(i*4+columnNum, R, G, B);
  }
}

void LedStrip::SwitchOffMatrixColumn(uint8_t columnNum) {
  SwitchOnMatrixColumn(columnNum, 0, 0, 0);
}

void LedStrip::SwitchOnMatrixLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B) {
  if (ledNum >= 16 || ledNum < 0) return;
  setLed(matrixLedPositions[ledNum], R, G, B);
}

void LedStrip::SwitchOffMatrixLed(uint8_t ledNum) {
  SwitchOnMatrixLed(ledNum, 0, 0, 0);
}

void LedStrip::SwitchOnButtonLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B) {
  if (ledNum >= 8 || ledNum < 0) return;
  setLed(buttonLedPositions[ledNum], R, G, B);
}

void LedStrip::SwitchOffButtonLed(uint8_t ledNum) {
  SwitchOnButtonLed(ledNum, 0, 0, 0);
}

void LedStrip::SwitchOnKnobLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B) {
  if (ledNum >= 3 || ledNum < 0) return;
  setLed(knobLedPositions[ledNum], R, G, B);
}

void LedStrip::SwitchOffKnobLed(uint8_t ledNum) {
  SwitchOnKnobLed(ledNum, 0, 0, 0);
}

void LedStrip::SwitchOnIoLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B) {
  if (ledNum >= 9 || ledNum < 0) return;
  setLed(ioLedPositions[ledNum], R, G, B);
}

void LedStrip::SwitchOffIoLed(uint8_t ledNum) {
  SwitchOnIoLed(ledNum, 0, 0, 0);
}

void LedStrip::setLed(uint8_t ledNum, uint8_t R, uint8_t G, uint8_t B) {
  strip.setPixelColor(ledNum, strip.Color(R, G, B));
}

void LedStrip::SwitchOffAll() {
  int i;

  for(i = 0; i<8; i++) {
    SwitchOffButtonLed(i);
  }

  for(i = 0; i<3; i++) {
    SwitchOffKnobLed(i);
  }

  for(i = 0; i<16; i++) {
    SwitchOffMatrixLed(i);
  }

  for(i = 0; i<9; i++) {
    SwitchOffIoLed(i);
  }
}

void LedStrip::Show() {
  strip.show();
}
