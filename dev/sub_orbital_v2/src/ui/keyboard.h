#ifndef __UI_KEYBOARD__
#define __UI_KEYBOARD__

#include "../../defines.h"
#ifdef SUB_ORBITAL

#include <Wire.h>
#include <Arduino.h>

class Keyboard {
public:
  Keyboard();
  void Setup(uint8_t defaultKeyboardMode, voidFuncPtr interruptCallback);
  void Scan();
  void InterruptCallback();
  uint8_t GetKeyDown();
  uint8_t GetKeyUp();
  int GetCalls();
private:
  const int *_buttonMap;
  int _address;
  uint8_t _keyDown;
  uint8_t _keyUp;
  int _calls;
  bool _keyboardChanged;

  void clearFlag();
  uint8_t getState();
};

#endif
#endif
