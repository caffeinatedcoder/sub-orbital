#ifndef __UI_ENCODER__
#define __UI_ENCODER__

#include <Arduino.h>

class Encoder {
public:
  Encoder();
  void Setup(int pin1, int pin2, voidFuncPtr interruptCallback);
  void InterruptCallback();
  void Reset(int value);
  int GetValue();
  int GetCalls();
private:
  int _pin1;
  int _pin2;
  int _value;
  int _calls;
};

#endif
