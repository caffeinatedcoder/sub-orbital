#ifndef __UTILS__
#define __UTILS__

#include "../../defines.h"
#include <Arduino.h>

int exponential_moving_average(int value, int last);
int bpm_to_time(int bpm, int numerator, int denominator);
float m_to_f(int note);
float func_linear(int x, int w, int h);
float func_inverse_linear(int x, int w, int h);
float func_exp(int x, int beta, int w, int h);
float func_log(int x, int beta, int w, int h);
float func_slope(int x, int step, int r, int w, int h);

#endif
