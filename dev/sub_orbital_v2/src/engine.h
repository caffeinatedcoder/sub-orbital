#ifndef __SUB_ORBITAL_ENGINE__
#define __SUB_ORBITAL_ENGINE__

#include "../defines.h"
#ifdef SUB_ORBITAL

#include <Arduino.h>
#include <Arduino_APDS9960.h>
#include <Arduino_HTS221.h>

#include "./core/_include.h"
#include "./io/_include.h"
#include "./ui/_include.h"
#include "./utils/_include.h"

class Engine {
public:
  //singleton pattern
  static Engine *Instance();
  //end singleton pattern

  void Setup();
  void Loop();
  void OnKeyDown();

  ProgramSwitcher *programSwitcher;

  //board
  Oled *oled;
  LedStrip *ledStrip;
  Pot *potA;
  Pot *potB;
  Encoder *encoder;
  Keyboard *keyboard;
  Gate *gateOutA;
  Gate *gateOutB;
  Gate *gateInA;
  Gate *gateInB;
  Midi *midiOut;

private:
  // singleton pattern
  static Engine* _instance;
  Engine();
  ~Engine();
  Engine(const Engine&);
  Engine& operator=(const Engine&);
  // end singleton pattern

  static void encoderInterruptCallback();
  static void keyBoardInterruptCallback();
  static void gateInAInterruptCallback();
  static void gateInBInterruptCallback();

  bool _state;
  Timer *_mainTimer;
  uint8_t _lastKeyDown;
  uint8_t _lastKeyUp;

  void processKeyboard();
};

#endif
#endif
