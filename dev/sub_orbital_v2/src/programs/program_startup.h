#ifndef __PROGRAM_STARTUP__
#define __PROGRAM_STARTUP__

#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramStartup : public Program {
public:
  ProgramStartup();
  void Setup();
  void Loop();
private:
  Timer     *_timer;
  uint8_t   _buttonLedIndex;
  uint8_t   _knobLedIndex;
  uint8_t   _matrixLedIndex;
  uint8_t   _matrixRowIndex;
  uint8_t   _matrixColIndex;
  uint8_t   _matrixMode;
};

#endif
