#ifndef __PROGRAM_ENCODER_TESTER__
#define __PROGRAM_ENCODER_TESTER__

#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramEncoderTester : public Program {
public:
  ProgramEncoderTester();
  void Setup();
  void Loop();
  void Reset();
  void OnKeyDown(int key);
private:
};

#endif
