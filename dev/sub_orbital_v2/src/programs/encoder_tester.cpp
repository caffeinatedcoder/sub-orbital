#include "encoder_tester.h"

ProgramEncoderTester::ProgramEncoderTester() {
  Setup();
}

void ProgramEncoderTester::Setup() {

}

void ProgramEncoderTester::Loop() {
  Oled *oled = Engine::Instance()->oled;
  Encoder *encoder = Engine::Instance()->encoder;

  oled->SetScale(2);
  oled->DrawStringLine(0, "ENCODER");
  oled->DrawParamLine(1, "CALLS", encoder->GetCalls());
  oled->DrawParamLine(2, "VALUE", encoder->GetValue());
}

void ProgramEncoderTester::Reset() {
  Engine::Instance()->encoder->Reset(0);
}

void ProgramEncoderTester::OnKeyDown(int key) {
  if (key == 8) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}
