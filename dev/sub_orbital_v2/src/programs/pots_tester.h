#ifndef __PROGRAM_POTS_TESTER__
#define __PROGRAM_POTS_TESTER__

#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramPotTester : public Program {
public:
  ProgramPotTester();
  void Setup();
  void Loop();
  void Reset();
  void OnKeyDown(int key);
private:
};

#endif
