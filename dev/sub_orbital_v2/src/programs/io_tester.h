#ifndef __PROGRAM_IO_TESTER__
#define __PROGRAM_IO_TESTER__

#include "../../defines.h"
#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramIoTester : public Program {
public:
  ProgramIoTester();
  void Setup();
  void Run();
  void Loop();
  void Reset();
  void OnKeyDown(int keyDown);
  void OnKeyUp(int keyUp);
  void OnGateIn(uint8_t channel);
private:
  Timer *_timer;
  int _bpm;
  int _ms;

  uint8_t _step;
  uint8_t _track;

  uint8_t _matrixLed;
  bool _gateOutALed;
  bool _gateOutBLed;
  bool _midiOutLed;
  bool _gateInALed;
  bool _gateInBLed;

  bool _trackA[8];
  bool _trackB[8];
  bool _midiOut[8];
  int ledTest;
  bool _sync;
};

#endif
