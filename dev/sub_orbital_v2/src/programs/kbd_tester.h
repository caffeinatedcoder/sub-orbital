#ifndef __PROGRAM_KBD_TESTER__
#define __PROGRAM_KBD_TESTER__


#include "../../defines.h"
#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramKbdTester : public Program {
public:
  ProgramKbdTester();
  void Setup();
  void Loop();
  void Reset();
  void OnKeyDown(int keyDown);
  void OnKeyUp(int keyUp);
private:
  bool _keyboardStates[KEYBOARD_BUTTONS_SIZE];
  int _mode;
};

#endif
