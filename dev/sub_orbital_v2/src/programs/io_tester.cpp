#include "io_tester.h"

ProgramIoTester::ProgramIoTester() {
  Setup();
}

void ProgramIoTester::Setup() {
  _timer = new Timer();
  _sync = false;
  Reset();
}

void ProgramIoTester::Loop() {
  Oled *oled = Engine::Instance()->oled;
  LedStrip *ledStrip = Engine::Instance()->ledStrip;
  Keyboard *keyboard = Engine::Instance()->keyboard;
  Encoder *encoder = Engine::Instance()->encoder;
  Pot *potA = Engine::Instance()->potA;
  Pot *potB = Engine::Instance()->potB;

  oled->SetScale(1);
  oled->DrawStringLine(0, "IO TESTER");
  oled->DrawParamLine(1, "BPM", _bpm);

  if (_track == 0) {
    oled->DrawStringLine(2, "TRACK: GATE OUT A");

    for (int i = 0; i < 8; i++) {
      if (_trackA[i]) {
        ledStrip->SwitchOnButtonLed(i, 0, 128, 0);
      }
    }
    
  } else if (_track == 1) {
    oled->DrawStringLine(2, "TRACK: GATE OUT B");

    for (int i = 0; i < 8; i++) {
      if (_trackB[i]) {
        ledStrip->SwitchOnButtonLed(i, 0, 0, 128);
      }
    }
  } else if (_track == 2) {
    oled->DrawStringLine(2, "TRACK: MIDI OUT");

    for (int i = 0; i < 8; i++) {
      if (_midiOut[i]) {
        ledStrip->SwitchOnButtonLed(i, 128, 0, 0);
      }
    }
  }

  ledStrip->SwitchOnMatrixLed(_matrixLed, 128, 128, 128);

  if (_gateOutALed) {
    ledStrip->SwitchOnIoLed(3, 0, 128, 0);
  }

  if (_gateOutBLed) {
    ledStrip->SwitchOnIoLed(5, 0, 0, 128);
  }

  if (_midiOutLed) {
    ledStrip->SwitchOnIoLed(7, 128, 0, 0);
  }

  if (_gateInALed) {
    ledStrip->SwitchOnIoLed(2, 0, 128, 0);
    _gateInALed = false;
  }

  if (_gateInBLed) {
    ledStrip->SwitchOnIoLed(4, 0, 0, 128);
    _gateInBLed = false;
  }

  // UI INPUTS (ENCODER AND POTS)
  int prev = _bpm;
  _bpm = encoder->GetValue() + 120;
  if (prev != _bpm) {
    _ms = bpm_to_time(_bpm, 1, 8);
  }

  int r = potA->ReadAs4();
  if (r < 3) {
    _track = r;
  } else {
    _track = 2;
  }
}

void ProgramIoTester::Run() {
  unsigned long elapsed = _timer->GetElapsed();

  // OUTBOARD
  if (elapsed >= _ms) {
    _timer->Update();

    Gate *gateOutA = Engine::Instance()->gateOutA;
    Gate *gateOutB = Engine::Instance()->gateOutB;
    Midi *midiOut = Engine::Instance()->midiOut;

    _gateOutALed = false;
    _gateOutBLed = false;
    _midiOutLed = false;

    if (_trackA[_step]) {
      gateOutA->Out(HIGH);
      _gateOutALed = true;
    }

    if (_trackB[_step]) {
      gateOutB->Out(HIGH);
      _gateOutBLed = true;
    }

    if (_midiOut[_step]) {
      midiOut->NoteOn(1, 60, 100);
      _midiOutLed = true;
    } else {
      //midiOut->NoteOff(1, 60);
    }

    _matrixLed = _step;

    _step = (_step + 1) % 8;
  } else if (elapsed > (_ms * .5)) {
    Gate *gateOutA = Engine::Instance()->gateOutA;
    Gate *gateOutB = Engine::Instance()->gateOutB;

    gateOutA->Out(LOW);
    _gateOutALed = false;
    gateOutB->Out(LOW);
    _gateOutBLed = false;
  }
}

void ProgramIoTester::Reset() {
  _bpm = 120;
  _ms = bpm_to_time(_bpm, 1, 8);
  _step = 0;
  _track = 0;
  _matrixLed = _step;

  for (int i = 0; i < 8; i++) {
    _trackA[i] = false;
    _trackB[i] = false;
    _midiOut[i] = false;
  }

  _trackA[0] = true;
  _trackB[0] = true;
  _midiOut[0] = true;
}

void ProgramIoTester::OnKeyDown(int key) {
  if (key < 8) {

    if (_track == 0) {
      _trackA[key] = !_trackA[key];
    } else if (_track == 1) {
      _trackB[key] = !_trackB[key];
    } else if (_track == 2) {
      _midiOut[key] = !_midiOut[key];
    }

  } else if (key == 8) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}

void ProgramIoTester::OnGateIn(uint8_t channel) {
  if (channel == 0) {
    _gateInALed = true;
  } else if (channel == 1) {
    _gateInBLed = true;
  }
}

void ProgramIoTester::OnKeyUp(int key) {
}
