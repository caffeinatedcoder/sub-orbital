#include "kbd_tester.h"

ProgramKbdTester::ProgramKbdTester() {
  Setup();
}

void ProgramKbdTester::Setup() {
  _mode = KEYBOARD_MODE_MOMENTARY;
  Reset();
}

void ProgramKbdTester::Loop() {
  Oled *oled = Engine::Instance()->oled;
  LedStrip *ledStrip = Engine::Instance()->ledStrip;
  Keyboard *keyboard = Engine::Instance()->keyboard;
  Encoder *encoder = Engine::Instance()->encoder;

  oled->SetScale(2);
  oled->DrawStringLine(0, "KEYBOARD");
  oled->DrawStringLine(1, "- SELECT -");

  if (_mode == KEYBOARD_MODE_MOMENTARY) {
    oled->DrawStringLine(2, "MOMENTARY");
  } else if (_mode == KEYBOARD_MODE_TOGGLE) {
    oled->DrawStringLine(2, "TOGGLE");
  } else if (_mode == KEYBOARD_MODE_TRIGGER) {
    oled->DrawStringLine(2, "TRIGGER");
  }

  for (int i = 0; i < KEYBOARD_BUTTONS_SIZE; i++) {
    if (_keyboardStates[i]) {
      ledStrip->SwitchOnMatrixLed(i, 128, 0, 0);
    }
  }

  int prevMode = _mode;
  int r = encoder->GetValue() % 3;
  _mode = (r < 0) ? r + 3 : r;

  if (_mode!=prevMode) {
    Reset();
  }

  if (_mode == KEYBOARD_MODE_TRIGGER) {
    Reset();
  }
}

void ProgramKbdTester::Reset() {
  for (int i = 0; i < KEYBOARD_BUTTONS_SIZE; i++) {
    _keyboardStates[i] = false;
  }
}

void ProgramKbdTester::OnKeyDown(int key) {
  if (key < 8) {

    if (_mode == KEYBOARD_MODE_MOMENTARY) {
      _keyboardStates[key] = true;
    } else if (_mode == KEYBOARD_MODE_TOGGLE) {
      _keyboardStates[key] = !_keyboardStates[key];
    } else if (_mode == KEYBOARD_MODE_TRIGGER) {
      _keyboardStates[key] = true;
    }

  } else if (key == 8) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}

void ProgramKbdTester::OnKeyUp(int key) {
  if (key < 8) {

    if (_mode == KEYBOARD_MODE_MOMENTARY) {
      _keyboardStates[key] = false;
    }

  }
}
