#include "program_startup.h"

ProgramStartup::ProgramStartup() {
  Setup();
}

void ProgramStartup::Setup() {
  _timer = new Timer();
  _knobLedIndex = 0;
  _buttonLedIndex = 0;
  _matrixLedIndex = 0;
  _matrixRowIndex = 0;
  _matrixColIndex = 0;
  _matrixMode = 0;
}

void ProgramStartup::Loop() {
  Engine *engine = Engine::Instance();
  engine->oled->RenderStartupScreen();

  int i;

  for(i = 0; i<8; i++) {
    if (_buttonLedIndex == i) {
      engine->ledStrip->SwitchOnButtonLed(i, 128, 0, 0);
    } else {
      engine->ledStrip->SwitchOffButtonLed(i);
    }
  }

  for(i = 0; i<3; i++) {
    if (_knobLedIndex == i) {
      engine->ledStrip->SwitchOnKnobLed(i, 0, 128, 0);
    } else {
      engine->ledStrip->SwitchOffKnobLed(i);
    }
  }

  if (_matrixMode == 0) {
    for(i = 0; i<16; i++) {
      if (_matrixLedIndex == i) {
        engine->ledStrip->SwitchOnMatrixLed(i, 0, 0, 128);
      } else {
        engine->ledStrip->SwitchOffMatrixLed(i);
      }
    }

    _matrixLedIndex = (_matrixLedIndex + 1) % 16;

    if (_matrixLedIndex == 0) {
      _matrixMode = 1;
    }

  } else if (_matrixMode == 1) {
    for(i = 0; i<4; i++) {
      if (_matrixRowIndex == i) {
        engine->ledStrip->SwitchOnMatrixRow(i, 0, 128, 128);
      } else {
        engine->ledStrip->SwitchOffMatrixRow(i);
      }
    }

    _matrixRowIndex = (_matrixRowIndex + 1) % 4;

    if (_matrixRowIndex == 0) {
      _matrixMode = 2;
    }
  } else if (_matrixMode == 2) {
    for(i = 0; i<4; i++) {
      if (_matrixColIndex == i) {
        engine->ledStrip->SwitchOnMatrixColumn(i, 128, 0, 128);
      } else {
        engine->ledStrip->SwitchOffMatrixColumn(i);
      }
    }

    _matrixColIndex = (_matrixColIndex + 1) % 4;

    if (_matrixColIndex == 0) {
      _matrixMode = 0;
      engine->programSwitcher->NextProgram();
    }
  }

  _buttonLedIndex = (_buttonLedIndex + 1) % 8;
  _knobLedIndex = (_knobLedIndex + 1) % 3;
}
