#include "pots_tester.h"

ProgramPotTester::ProgramPotTester() {
  Setup();
}

void ProgramPotTester::Setup() {

}

void ProgramPotTester::Loop() {
  Oled *oled = Engine::Instance()->oled;
  Pot *potA = Engine::Instance()->potA;
  Pot *potB = Engine::Instance()->potB;

  oled->DrawParamLine(0, "POT A", potA->ReadRaw(), 0);
  oled->DrawParamLine(1, "0-4", potA->ReadAs4(), 0);
  oled->DrawParamLine(2, "0-8", potA->ReadAs8(), 0);
  oled->DrawParamLine(3, "0-16", potA->ReadAs16(), 0);
  oled->DrawParamLine(4, "0-64", potA->ReadAs64(), 0);
  oled->DrawParamLine(5, "0-128", potA->ReadAs128(), 0);

  oled->DrawParamLine(0, "POT B", potB->ReadRaw(), 1);
  oled->DrawParamLine(1, "0-4", potB->ReadAs4(), 1);
  oled->DrawParamLine(2, "0-8", potB->ReadAs8(), 1);
  oled->DrawParamLine(3, "0-16", potB->ReadAs16(), 1);
  oled->DrawParamLine(4, "0-64", potB->ReadAs64(), 1);
  oled->DrawParamLine(5, "0-128", potB->ReadAs128(), 1);
}

void ProgramPotTester::Reset() {
}

void ProgramPotTester::OnKeyDown(int key) {
  if (key == 8) {
    Engine::Instance()->programSwitcher->NextProgram();
  }
}
