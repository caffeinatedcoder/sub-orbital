#include "engine.h"

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();
	_state = false;

	programSwitcher = new ProgramSwitcher();

	// board
	oled = new Oled();
	ledStrip = new LedStrip();
	potA = new Pot();
	potB = new Pot();
	encoder = new Encoder();
	keyboard = new Keyboard();
	gateOutA = new Gate();
	gateOutB = new Gate();
	gateInA = new Gate();
	gateInB = new Gate();
	midiOut = new Midi();
}

void Engine::Setup() {
	//board IO
  pinMode(LED_BUILTIN, OUTPUT);

	_mainTimer->Setup();

	oled->Setup();
	ledStrip->Setup();
	potA->Setup(ANALOG_POT_PIN_1, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
	potB->Setup(ANALOG_POT_PIN_2, true, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
	encoder->Setup(ENCODER_PIN_A, ENCODER_PIN_B, encoderInterruptCallback);
	keyboard->Setup(KEYBOARD_MODE_MOMENTARY, keyBoardInterruptCallback);
	gateOutA->Setup(GATE_A_OUT_PIN, OUTPUT);
	gateOutB->Setup(GATE_B_OUT_PIN, OUTPUT);
	gateInA->Setup(GATE_A_IN_PIN, INPUT_PULLUP, gateInAInterruptCallback);
	gateInB->Setup(GATE_B_IN_PIN, INPUT_PULLUP, gateInBInterruptCallback);
	midiOut->Setup();

	// keyboard debouncing
	_lastKeyDown = keyboard->GetKeyDown();
	_lastKeyUp = keyboard->GetKeyUp();
}

void Engine::Loop() {
	ledStrip->SwitchOffAll();
	
	// execute program not constrained to FPS
	if (programSwitcher->GetCurrentProgram() != NULL) {
		programSwitcher->GetCurrentProgram()->Run();
	}

  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		processKeyboard();

		oled->ClearDisplay();


		// execute program
		if (programSwitcher->GetCurrentProgram() != NULL) {
			programSwitcher->GetCurrentProgram()->Loop();
		}

		oled->Render();
		ledStrip->Show();

		// blink the arduino builtin led, so we can check it is not crashed
		if (_state) {
	    digitalWrite(LED_BUILTIN, HIGH);
	  } else {
	    digitalWrite(LED_BUILTIN, LOW);
	  }

		_state = !_state;
  }
}

void Engine::processKeyboard() {
	keyboard->Scan();

  int keyDown = keyboard->GetKeyDown();
	int keyUp = keyboard->GetKeyUp();

  if (keyDown < 255) {
    if (keyDown != _lastKeyDown) {
			_lastKeyDown = keyDown;
			programSwitcher->GetCurrentProgram()->OnKeyDown(keyDown);
    }
  } else {
    _lastKeyDown = keyDown;
  }

	if (keyUp < 255) {
    if (keyUp != _lastKeyUp) {
			_lastKeyUp = keyUp;
			programSwitcher->GetCurrentProgram()->OnKeyUp(keyUp);
    }
  } else {
    _lastKeyUp = keyUp;
  }

}

void Engine::OnKeyDown() {
	_lastKeyDown = keyboard->GetKeyDown();
	_lastKeyUp = keyboard->GetKeyUp();
	keyboard->InterruptCallback();
}

void Engine::encoderInterruptCallback() {
	Engine::Instance()->encoder->InterruptCallback();
}

void Engine::keyBoardInterruptCallback() {
	Engine::Instance()->OnKeyDown();
}

void Engine::gateInAInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(0);
}

void Engine::gateInBInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(1);
}
