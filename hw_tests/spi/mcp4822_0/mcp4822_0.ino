// https://github.com/SteveGdvs/MCP48xx/blob/master/src/MCP48xx.h

#include <Arduino.h>
#include <SPI.h>

#define BITS_RES_MCP4822 12
#define BITS_RES_MCP4812 10
#define BITS_RES_MCP4802 8

#define SPI_MISO_PIN 12
#define SPI_MOSI_PIN 11
#define SPI_SCK_PIN 13
#define SPI_LATO_HVC_PIN A2
#define SPI_CS_PIN 10

const uint8_t channelA = 0u;
const uint8_t channelB = 1u;

const uint8_t gainHigh = 0u;
const uint8_t gainLow = 1u;
const uint8_t bitsRes = BITS_RES_MCP4822;

uint16_t commandA = 0 | (channelA << 15u);
uint16_t commandB = 0 | (channelB << 15u);

uint8_t gain = gainHigh;
int value = 10000;

void setup() {
  pinMode(SPI_CS_PIN, OUTPUT);
  digitalWrite(SPI_CS_PIN, HIGH);
  SPI.begin();

  // turn on channels
  commandA = commandA | (1u << 12u);
  commandB = commandB | (1u << 12u);

  // set gain
  commandA = commandA | (gain << 13u);
  commandB = commandB | (gain << 13u);
}

void loop() {
  value += 100;

  if (value > (1u << bitsRes) - 1) {
    value = (1u << bitsRes) - 1;
  } else {
    value = value >> (12u - bitsRes);
  }
  commandA = commandA & 0xF000u;
  commandA = commandA | value;

  commandB = commandB & 0xF000u;
  commandB = commandB | value;

  /* begin transaction using maximum clock frequency of 20MHz */
  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
  digitalWrite(SPI_CS_PIN, LOW); //select device
  SPI.transfer16(commandA); // sent command for the A channel
  digitalWrite(SPI_CS_PIN, HIGH); //deselect device
  SPI.endTransaction();

  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
  digitalWrite(SPI_CS_PIN, LOW); //select device
  SPI.transfer16(commandB); // sent command for the A channel
  digitalWrite(SPI_CS_PIN, HIGH); //deselect device
  SPI.endTransaction();

  delay(1000);
}
