// https://forum.arduino.cc/index.php?topic=235182.0

#include <Arduino.h>
#include <SPI.h>

#define SPI_MISO_PIN 12
#define SPI_MOSI_PIN 11
#define SPI_SCK_PIN 13
#define SPI_LATO_HVC_PIN A2
#define SPI_CS_PIN 10

void setup() {
  pinMode(SPI_CS_PIN, OUTPUT);
 	digitalWrite(SPI_CS_PIN, HIGH);
 	SPI.begin();
}

void loop() {
  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
  digitalWrite(SPI_CS_PIN, LOW);
	SPI.transfer(B01111000);
	SPI.transfer(B00000000);
	digitalWrite(SPI_CS_PIN, HIGH);
}
