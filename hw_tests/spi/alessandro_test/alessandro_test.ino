// FASELUNARE MCP4822 TEST

//#define SPI_MISO_PIN 12
#define SPI_MOSI_PIN 11
#define SPI_SCK_PIN 13
//#define SPI_LATO_HVC_PIN A2
#define SPI_CS_PIN 10
#include <Arduino.h>
#include <SPI.h>
#include "MCP48xx.h"

#define BITS_RES_MCP4822 12
#define BITS_RES_MCP4812 10
#define BITS_RES_MCP4802 8
MCP4822 dac(10);

int tic;
int maxTic;
int minVoltage;
int maxVoltage;

// We define an int variable to store the voltage in mV so 100mV = 0.1V
int voltage;

void setup() {
  pinMode(SPI_CS_PIN, OUTPUT);
  digitalWrite(SPI_CS_PIN, HIGH);

  SPI.begin();
  /* begin transaction using maximum clock frequency of 1 MHz */
  // SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));

  // We call the init() method to initialize the instance
  dac.init();
  // The channels are turned off at startup so we need to turn the channel we need on
  dac.turnOnChannelA();
  dac.turnOnChannelB();
  // We configure the channels in High gain
  // It is also the default value so it is not really needed
  dac.setGainA(MCP4822::High);
  dac.setGainB(MCP4822::High);

  Serial.begin(115200);
  delay(400);
  Serial.println("MCP4822 TEST");
  delay(300);
  Serial.println("Author: FaseLunare");

  tic = 0;
  minVoltage = 100;
  maxVoltage = 4000;
  maxTic = maxVoltage - minVoltage;
}
// We loop from 100mV to 2000mV for channel A and 4000mV for channel B
void loop() {
  // SAW UP
  // voltage = (tic % maxTic) + minVoltage;

  // TRIANGLE
  voltage = abs((tic % maxTic) - maxTic * 0.5) + minVoltage;

  // We set channel A to output 500mV
  dac.setVoltageA(voltage);
  // We set channel B to output 1000mV
  dac.setVoltageB(voltage * 2);
  // We send the command to the MCP4822
  // This is needed every time we make any change

  dac.updateDAC();

  Serial.println(voltage);

  //delay(10);

  tic++;
}
